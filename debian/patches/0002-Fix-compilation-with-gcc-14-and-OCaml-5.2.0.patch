From: Stephane Glondu <steph@glondu.net>
Date: Tue, 6 Aug 2024 12:07:56 +0200
Subject: Fix compilation with gcc-14 and OCaml 5.2.0

---
 Togl/src/Togl/togl.c |  2 +-
 Togl/src/ml_togl.c   | 22 +++++++++++-----------
 2 files changed, 12 insertions(+), 12 deletions(-)

diff --git a/Togl/src/Togl/togl.c b/Togl/src/Togl/togl.c
index 0b71b99..227e7be 100644
--- a/Togl/src/Togl/togl.c
+++ b/Togl/src/Togl/togl.c
@@ -96,7 +96,7 @@
 /* pointer to Tk_SetClassProcs function in the stub table */
 
 static void (*SetClassProcsPtr)
-        _ANSI_ARGS_((Tk_Window, Tk_ClassProcs *, ClientData));
+        _ANSI_ARGS_((Tk_Window, const Tk_ClassProcs *, ClientData));
 #endif
 
 /* 
diff --git a/Togl/src/ml_togl.c b/Togl/src/ml_togl.c
index 2f833db..976b5c6 100644
--- a/Togl/src/ml_togl.c
+++ b/Togl/src/ml_togl.c
@@ -31,25 +31,25 @@ int TOGLenum_val(value tag)
     {
 #include "togl_tags.c"
     }
-    invalid_argument ("Unknown Togl tag");
+    caml_invalid_argument ("Unknown Togl tag");
 }
 
 /* Avoid direct use of stderr */
 void togl_prerr(const char *msg)
 {
-    value ml_msg = copy_string(msg);
-    value *prerr = caml_named_value("togl_prerr");
+    value ml_msg = caml_copy_string(msg);
+    const value *prerr = caml_named_value("togl_prerr");
     if (!prerr) caml_failwith(msg);
     caml_callback_exn(*prerr, ml_msg);
 }
 
 CAMLprim value ml_Togl_Init (value unit)  /* ML */
 {
-    value *interp = caml_named_value("cltclinterp");
+    const value *interp = caml_named_value("cltclinterp");
     Tcl_Interp *cltclinterp =
       (interp ? (Tcl_Interp *) Nativeint_val(Field(*interp,0)) : NULL);
     if (cltclinterp == NULL || Togl_Init(cltclinterp) == TCL_ERROR)
-      raise_with_string(*caml_named_value("tkerror"), "Togl_Init failed");
+      caml_raise_with_string(*caml_named_value("tkerror"), "Togl_Init failed");
     return Val_unit;
 }
 
@@ -73,14 +73,14 @@ enum {
      LastFunc
 };
 
-static value *callbacks = NULL;
+static const value *callbacks = NULL;
 
 #define CALLBACK(func) \
 static void callback_##func (struct Togl *togl) \
-{ callback (Field(*callbacks, func), Val_addr(togl)); }
+{ caml_callback (Field(*callbacks, func), Val_addr(togl)); }
 #define CALLBACK_const(func) \
 static void callback_##func (const struct Togl *togl) \
-{ callback (Field(*callbacks, func), Val_addr(togl)); }
+{ caml_callback (Field(*callbacks, func), Val_addr(togl)); }
 
 #define ENABLER(func) \
 CAMLprim value ml_Togl_##func (value unit) \
@@ -106,7 +106,7 @@ ENABLER (OverlayDisplayFunc)
 ML_0 (Togl_ResetDefaultCallbacks)
 ML_1 (Togl_PostRedisplay, Addr_val)
 ML_1 (Togl_SwapBuffers, Addr_val)
-ML_1_ (Togl_Ident, Addr_val, copy_string)
+ML_1_ (Togl_Ident, Addr_val, caml_copy_string)
 ML_1_ (Togl_Width, Addr_val, Val_int)
 ML_1_ (Togl_Height, Addr_val, Val_int)
 
@@ -131,7 +131,7 @@ ML_2 (Togl_UnloadBitmapFont, Addr_val, Int_val)
 ML_2 (Togl_UseLayer, Addr_val, TOGLenum_val)
 #ifdef _WIN32
 CAMLprim value ml_Togl_ShowOverlay(value v)
-{ invalid_argument("Togl_ShowOverlay: not implemented"); return Val_unit; }
+{ caml_invalid_argument("Togl_ShowOverlay: not implemented"); return Val_unit; }
 #else
 ML_1 (Togl_ShowOverlay, Addr_val)
 #endif
@@ -146,7 +146,7 @@ CAMLprim value ml_Togl_DumpToEpsFile (value togl, value filename, value rgb)
     if (Togl_DumpToEpsFile(Addr_val(togl), String_val(filename),
 			   Int_val(rgb), callback_RenderFunc)
 	== TCL_ERROR)
-        raise_with_string(*caml_named_value("tkerror"),
+        caml_raise_with_string(*caml_named_value("tkerror"),
                           "Dump to EPS file failed");
     return Val_unit;
 }
